<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
<body>
<?php
function is_multiple3($value){
    $type = gettype($value);
    if($value%3 == 0){
        echo $value. 'は３の倍数です。<br>';
    }else{
        echo $value. 'は３の倍数ではありません。<br>';
    }
}
is_multiple3(4);
is_multiple3(3);
is_multiple3(2);
is_multiple3(1);
is_multiple3(0);
is_multiple3(-1);
is_multiple3(-2);
is_multiple3(-3);
is_multiple3(-4);
is_multiple3('abc');
    
?>
</body>
</html>
