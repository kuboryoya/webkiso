<?php
//ユーザーリスト
function get_users(){
    $date = file_get_contents('user.json');
    if ($date === false) {
        echo 'データが読み込めませんでした。';
        return array();
    } else {
        return json_decode($date, true);
    }
}

function add_users($name, $pass){
    $users = get_users();
    $users [] = [ 'id' => $name, 'pass' => $pass ];
    $json = json_encode($users, JSON_PRETTY_PRINT);
    file_put_contents('user.json', $json);
}

function password_check($name, $pass){
    foreach(get_users() as $user){
        if ($user['id'] == $name &&
            $user['pass'] == $pass ) {
            //ユーザー名とパスワードがあっていた
            return true;
        }
    }
    return false;
} 
//
////ユーザー名とパスワードが一致
//echo password_check('ske', 'ske48')? 'OK' : 'NG';
//echo '<br>';
//echo password_check('ske', 'ske47')? 'OK' : 'NG';
