<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php

function is_half($timestamp = null){
    
    //$timestampが渡されていなかったら現在のタイムスタンプを代入
    if ($timestamp === null){
        $timestamp = time();
    }
    echo date('Y/m/d H:i:s', $timestamp) .'<br>';
    $day = date('j', $timestamp);
    echo $day.'日 : ';
    
    //三項演算子で表示
    $message = ($day <= 15)?'前半です。' : '後半です。';
    echo $message.'<br><br>';
}

$timestamp = mktime(0,0,0,06,1,2017);
$timestamp2 = mktime(0,0,0,06,15,2017);

is_half();
is_half($timestamp);
is_half($timestamp2);

?>
</body>
</html>
