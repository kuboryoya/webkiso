<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php

$names = ['浅野', '伊藤', '宇田', '江本'];
foreach ($names as $name){
    echo $name. '<br>';
}

echo '<hr>';

$names['oota'] = '太田';
foreach ($names as $name){
    echo $name. '<br>';
}

echo '<hr>';

foreach ($names as $key => $name){
    echo $key. ':' . $name. '<br>';
}

?>
    </body>
</html>
