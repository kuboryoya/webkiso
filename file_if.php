<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$point     = 39;
$line      = 80;
$underline = 40;

if ($point >= $line) {
    echo '合格ライン';
}elseif ($point >= $underline) {
    echo '追試ライン';
}else {
    echo '落第ライン';
}

echo '<br>';
$kuji = false;
if ($kuji == true) {
    echo '当選1<br>';
    echo '当選1<br>';
}

if ($kuji == true)     //{}無しでは複数の命令ができない。
    echo '当選2<br>';
    echo '当選2<br>';   //この行は条件無しに実行される

if ($kuji == true) { 
    echo '当選3<br>';
}
if ($kuji) {
    echo '当選3<br>';
}

if ($kuji != true) {
    echo '落選1<br>';
}

if (!$kuji){
    echo '落選2<br>';
}

$val = 1;
if ($val = 2) { // =を１つにした場合、代入が実行され、この条件はtrueになる。
    echo $val;
}

?>
    </body>
</html>
