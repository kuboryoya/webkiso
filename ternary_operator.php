<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$language = 'jp';
$message = ($language == 'jp')?'日本語' : 'Japanese';
echo $message. '<br>';

//三項演算子の省略形です。
$message = $message ?:'メッセージは空(1)';
echo $message .'<br>';
$message = '';
$message = $message ?:'メッセージは空(2)';
echo $message .'<br>'

?>
    </body>
</html>
