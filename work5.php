<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
<body>
<?php

function get_next_year(){
$next_year = date('Y') + 1;
return $next_year;
}

$birth_year = 1997;

echo '私は' . $birth_year . '年生まれです。来園は' . (get_next_year() - $birth_year) .'歳です。';

?>
</body>
</html>
