<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php


//複数の引数
//$plus1 = 1011;
//$plus2 = 10;
//
//function plus12($p1, $p2){
//  $plus3 = $p1 + $p2;
//  return $plus3;
//}
//
//echo plus12($plus1, $plus2);


//デフォルト値
function convert_to_tsubo ($area = 10){
    $tsubo  = $area * 0.3025;
    return $tsubo;
}

//$area = 1000;
//echo '$areaの初期値は' .$area . 'です。<br>';
$result = convert_to_tsubo();
//echo '関数に渡された$areaの値は'. $area . 'です。<br>';

echo '関数の実行結果は' . $result.'です。';
?>
    </body>
</html>
