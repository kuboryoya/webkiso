<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php

$a = 'グローバル$a';
$b = 'グローバル$b';

function test(){
    $a = 'ローカル$a';
    echo $a .'<br>';

    global $b;
    $b = 'ローカル$b';
    echo $b . '<br>';
}

test();
echo $a. '<br>';
echo $b;

?>
    </body>
</html>
