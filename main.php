<?php
include_once 'login.php';

if (isset($_POST['user_add'])){
    if(!empty($_POST['username']) && !empty($_POST['data'])){
        //ユーザーを追加
        add_users($_POST['username'], $_POST['data']);
        //メッセージ
        $message = 'ユーザーを追加しました。';
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>

<?php if ($_SESSION['login']): ?>
        <?= $_SESSION['username'] ?> さん、ようこそ！
        <hr>
        秘密の情報 
        <a href="logout.php">ログアウト</a>
        <hr>
        <?php
        if(!empty($message)){
            echo $message.'<br>';
        }
        ?>
        ここからユーザーを追加できます。
        <form method="post" action="" enctypenctype="multipart/form-data">
            ユーザー名 <input type="text" name="username"><br>
            パスワード <input type="password" name="data"><br>
            <input type="submit" value="ユーザ追加" name="user_add">
        </form>
<?php endif;?>
    </body>
</html>
