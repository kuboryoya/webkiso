<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$timestamp = time();
week($timestamp);
$timestamp = mktime(0,0,0,06,16,2017);
week($timestamp);
$timestamp = mktime(0,0,0,06,18,2017);
week($timestamp);
function week($timestamp = null){
    
    if ($timestamp === null){
        $timestamp = time();
    }
    
    echo date('Y/m/d', $timestamp) .'<br>';
    $week_num = date('w', $timestamp);
    switch ($week_num){
        case '1':
            echo '月曜日です。';
            break;
        case '2':
            echo '火曜日です。';
            break;
        case '3':
            echo '水曜日です。';
            break;
        case '4':
            echo '木曜日です。';
            break;
        case '5':
            echo '金曜日です。';
            break;
        default :
            echo 'お休みです。';
            break;
    }
    echo '<br>';
}
?>
    </body>
</html>
