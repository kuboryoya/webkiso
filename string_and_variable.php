<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <title>変数と文字列の出力</title>
</head>
<body>
<?php
$name ='PHP逆引きレシピ';
$Name = 'CodeIgniter徹底入門';

echo '書籍名: '. $name .'<br>';
echo "書籍名: {$Name}\n<br>";
echo '書籍名: {$Name}\n<br>';

$format = '書籍名: %s %s <br>';
echo sprintf($format, $name, $Name);

//可読性を考え、複数行のテキストを入れる。
$book = 'PHP逆引きレシピ';
$text = <<<EOL
ヒアドキュメントで変数に文章を代入します。<br>
書籍名 : $book<br>
EOL;

echo $text;
?>
</body>
</html>
