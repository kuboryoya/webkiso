<?php
session_set_cookie_params(3600);
session_start();

//セッションクリア
$SESSION = array();
session_destroy();

//自動でページ遷移
header('location: main.php');

