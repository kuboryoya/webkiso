<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$a = 123;
$b = '321';
$c = '1234円';
$c = intval($c);

echo $c . '<br>';

echo '$a の型; ' .gettype($a) . '<br>';
echo '$b の型; ' .gettype($b) . '<br><br>';

$a = strval($a);
$b = intval($b);

echo '$a の型; ' .gettype($a) . '<br>';
echo '$b の型; ' .gettype($b) . '<br><br>';

$a = (int) $a;
$b = (string) $b;

echo '$a の型; ' .gettype($a) . '<br>';
echo '$b の型; ' .gettype($b) . '<br><br>';
?>
    </body>
</html>
