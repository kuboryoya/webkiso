<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
$a = null;
$b = '';

echo 'a: ' .$a . '<br>';
echo 'b: ' .$b . '<br>';

if($a == $b){
    echo '$a と $b は 等しい(==)<br>';
}else{
    echo '$a と $b は 異なる(==)<br>';
}

if($a === $b){
    echo '$a と $b は 等しい(===)<br>';
}else{
    echo '$a と $b は 異なる(===)<br>';
}

if(is_null($a)){
   echo '$a は null です<br>';
}else{
   echo '$a は null ではありません<br>';
}

if(isset($a)){
    echo '$a は 定義されています<br>';
}else{
    echo '$a は 定義されていません<br>';
}



?>
    </body>
</html>
