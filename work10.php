<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php

function is_weekday($timestamp = null){
    
    //$timestampが渡されていなかったら現在のタイムスタンプを代入
    if ($timestamp === null){
        $timestamp = time();
    }
    echo date('Y/m/d H:i:s', $timestamp) .'<br>';
    $week = date('w', $timestamp);
    
    //if文で表示
    if($week != 0 && $week != 6){
        echo '平日です。<br>';
    }else{
        echo 'お休みです。<br>';
    }
    
    //三項演算子で表示
    $message = ($week != 0 && $week != 6)?'平日です。' : 'お休みです';
    echo $message.'<br><br>';
}

$timestamp = time();
$timestamp2 = mktime(0,0,0,06,18,2017);

is_weekday();
is_weekday($timestamp);
is_weekday($timestamp2);

?>
</body>
</html>
