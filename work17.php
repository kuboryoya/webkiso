<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form method="post" action=""
            enctype="multipart/form-data">
            <input type="text" name="max">
            <input type="submit" value="送信">            
        </form>
<?php

$max = (isset($_POST['max']))? $_POST['max'] : '';

for($i = 1; $i <= $max; $i++) {
    echo three_five($i) . ' ';
}

function three_five($num) {
    if($num % 3 == 0 && $num % 5 == 0) {
        return 'fifteen';
    }elseif($num % 3 == 0) {
        return 'three';
    }elseif($num % 5 == 0) {
        return 'five';
    }else {
        return $num;
    }
}

?>
    </body>
</html>
