<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
function is_seinen($age){
    if(gettype($age) == 'integer'){
        if($age < 20){
            echo '未成年です<br>';
        }elseif($age == 20){
            echo '成年しました<br>';
        }else{
            echo '成年済みです。<br>';
        }
    }else{
        echo $age.'は無効なデータ型です。<br>';
    }
}
is_seinen(true);
is_seinen(false);
is_seinen('abc');
is_seinen(19);
is_seinen(20);
is_seinen(21);

?>
    </body>
</html>
