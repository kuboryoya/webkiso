<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php

function is_sunday($timestamp = null){
    
    //$timestampが渡されていなかったら現在のタイムスタンプを代入
    if ($timestamp === null){
        $timestamp = time();
    }
    echo $timestamp .'<br>';
    echo date('Y/m/d H:i:s', $timestamp) .'<br>';
    $week = date('w', $timestamp);
    if($week == 0){
        echo '日曜日です。<br>';
    }else{
        echo '日曜日ではありません。<br><br>';
    }
}

$timestamp = time();
$timestamp2 = mktime(0,0,0,06,18,2017);

is_sunday();
is_sunday($timestamp);
is_sunday($timestamp2);
?>
    </body>
</html>
